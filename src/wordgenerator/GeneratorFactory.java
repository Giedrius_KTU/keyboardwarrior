package wordgenerator;

import state.Type;


public class GeneratorFactory {
    public static Generator buildGenerator(Type t) {
        switch (t) {
        case ONECHAR:
            return new SingleCharGenerator();
        case WHOLEWORD:
            return new WholeWordGenerator();
        default:
            return null;
        }
    }
}
