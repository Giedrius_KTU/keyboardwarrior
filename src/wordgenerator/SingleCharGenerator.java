package wordgenerator;

import java.util.Random;

public class SingleCharGenerator implements Generator {
    private final String charlist;
    private final Random random;

    public SingleCharGenerator() {
        this.charlist = "ąčęėįšųūžqwertyuiopasdfghjklzxcvbnm";
        this.random = new Random(System.currentTimeMillis());
    }
    
    @Override
    public String getWord() {
        boolean uppercase = this.random.nextBoolean();
        int index = this.random.nextInt(this.charlist.length());
        
        if (uppercase)
            return String.valueOf(Character.toUpperCase(this.charlist.charAt(index)));
        else
            return String.valueOf(this.charlist.charAt(index));
    }
}
