package wordgenerator;

public interface Generator {
    public String getWord();
}
