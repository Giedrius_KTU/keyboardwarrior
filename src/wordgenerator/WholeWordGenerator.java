package wordgenerator;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import util.*;

public class WholeWordGenerator implements Generator {
    private final List<String> wordlist;
    private final Random random;
    private final String filename;

    public WholeWordGenerator() {
        this.random = new Random(System.currentTimeMillis());
        this.wordlist = new ArrayList<>();
        this.filename = "words.txt";
        
        try {
            ParseWords();
        } catch (FileNotFoundException ex) {
            Logger.getInstance().error("can't open " + this.filename);
            Logger.getInstance().info("adding dummy words");
            this.wordlist.add("hello");
            this.wordlist.add("world");
        }
    }
    
    private void ParseWords() throws FileNotFoundException {
        Scanner scan = new Scanner(new File(this.filename));
        while (scan.hasNextLine()) {
            String word = scan.nextLine();
            if (word.length() == 0)
                continue;
            this.wordlist.add(word);
        }
        scan.close();
    }
    
    @Override
    public String getWord() {
        int index = this.random.nextInt(this.wordlist.size());
        return this.wordlist.get(index);
    }
}
