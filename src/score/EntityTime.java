package score;

import wordgenerator.Generator;

public class EntityTime extends Entity {
    private final long startTime;
    
    public EntityTime(Generator gen) {
        super(gen);
        this.startTime = System.currentTimeMillis();
    }
    
    public long passedTime() {
        return System.currentTimeMillis() - this.startTime;
    }
    
    @Override
    public long getPoints() {
        long pts = super.getPoints() - passedTime();
        return pts < 0 ? 0 : pts;
    }
}
