package score;

public class HighscoreLink {
    public HighscoreLink succ = null;
    public HighscoreEntry e = null;
    
    public HighscoreLink(HighscoreLink succ) {
        this.succ = succ;
    }
    
    public HighscoreEntry getEntry() {
        return e;
    }
    
    public void setEntry(HighscoreEntry e) {
        this.e = e;
    }
    
    public HighscoreLink getSuccessor() {
        return succ;
    }
    
    public boolean saveEntry(HighscoreLink l) {
        if (e != null && l.getEntry().getScore() >= e.getScore()) {
            String currname;
            Integer currscore;
            HighscoreLink s = succ;
            
            while (s != null) {
                currname = s.getEntry().getName();
                currscore = s.getEntry().getScore();
                s.getEntry().setName(currname);
                s.getEntry().setScore(currscore);
                s = s.getSuccessor();
            }
            
            e.setName(l.getEntry().getName());
            e.setScore(l.getEntry().getScore());
            return true;
        } else {
            if (succ == null)
                return false;
            else
                return succ.saveEntry(l);
        }
    }
}
