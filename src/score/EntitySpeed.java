package score;

import wordgenerator.Generator;

public class EntitySpeed extends Entity {
    private Integer speed;
    
    public EntitySpeed(Generator gen) {
        super(gen);
        this.speed = 1;
    }
    
    public void increaseSpeed() {
        this.speed += 1;
    }
    
    public Integer getSpeed() {
        return this.speed;
    }
    
    @Override
    public long getPoints() {
        return this.speed + super.getPoints();
    }
    
}
