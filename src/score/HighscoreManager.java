package score;

import general.Container;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Iterator;
import util.*;

public class HighscoreManager implements Container {
    private HighscoreEntry entrylist[];
    private HighscoreLink linklist[];
    private final String hsfilename;
    private final Integer MAX_ENTRIES = 10;
    
    public HighscoreManager(String filename) {
        this.hsfilename = filename;
        this.entrylist = new HighscoreEntry[MAX_ENTRIES];
        this.linklist = new HighscoreLink[MAX_ENTRIES];
        
        
        linklist[MAX_ENTRIES-1] = new HighscoreLink(null);
        for (int i = MAX_ENTRIES-2; i >= 0; i--) {
            linklist[i] = new HighscoreLink(linklist[i+1]);
        }
    }
    
    public void ReadScore() {
        try {
            FileInputStream fis = new FileInputStream(this.hsfilename);
            ObjectInputStream ois = new ObjectInputStream(fis);
            this.entrylist = (HighscoreEntry[]) ois.readObject();
            for (int i = 0; i < MAX_ENTRIES; i++) {
                this.linklist[i].setEntry(this.entrylist[i]);
            }
            ois.close();
            fis.close();
        } catch (IOException | ClassNotFoundException ex) {
            Logger.getInstance().error("can't open " + this.hsfilename);
        }
    }
    
    public void SaveScore() {
        try {
            FileOutputStream fos = new FileOutputStream(this.hsfilename);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(entrylist);
            oos.close();
            fos.close();
        } catch (IOException ex) {
            Logger.getInstance().error("can't open " + this.hsfilename);
        }
    }
    
    
    
    public void SetEntry(Integer index, HighscoreEntry entry) {
        if (index < 0 || index > MAX_ENTRIES) {
            Logger.getInstance().error("index out of bounds");
            return;
        }
        entrylist[index] = entry;
        linklist[index].setEntry(entry);
    }
    
    public HighscoreEntry GetEntry(Integer index) {
        if (index < 0 || index > MAX_ENTRIES) {
            Logger.getInstance().error("index out of bounds");
            return null;
        }
        return entrylist[index];
    }
    
    public boolean SaveEntry(HighscoreEntry entry) {
        HighscoreLink l = new HighscoreLink(null);
        l.setEntry(entry);
        return linklist[0].saveEntry(l);
    }
    
    private class HighscoreIterator implements Iterator {
        int index;
        
        @Override
        public boolean hasNext() {
            return index < entrylist.length;
        }

        @Override
        public HighscoreEntry next() {
            if (this.hasNext())
                return entrylist[index++];
            return null;
        }
        
    }
    @Override
    public Iterator getIterator() {
        return new HighscoreIterator();
    }
    
    
}
