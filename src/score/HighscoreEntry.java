package score;

public class HighscoreEntry implements java.io.Serializable {
    public String name;
    public Integer score;
    
    public HighscoreEntry(String name, Integer score) {
        this.name = name;
        this.score = score;
    }
    
    public String toString() {
        return "Name " + this.name + ", score: " + this.score;
    }
    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public Integer getScore() {
        return score;
    }
    
    public void setScore(Integer score) {
        this.score = score;
    }
}
