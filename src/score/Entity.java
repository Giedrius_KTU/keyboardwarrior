package score;

import wordgenerator.Generator;

public class Entity {
    private final Generator gen;
    private String word;
    
    public Entity(Generator gen) {
        this.gen = gen;
    }
    
    public void getWord() {
        this.word = this.gen.getWord();
    }
    
    public long getPoints() {
        if ("".equals(this.word))
            getWord();
        return 0;
    }
    
}
