package keyboardwarrior;

import java.util.Iterator;
import score.*;
import state.*;
import util.Logger;
import util.TimeOn;
import wordgenerator.*;


public class KeyboardWarrior {
    public static void main(String[] args) {
        Generator g1;
        HighscoreManager hm = new HighscoreManager("highscore");
        Game g = new Game(true, Type.ONECHAR);
        GameMutator gm = new GameMutator();
        
        g1 = GeneratorFactory.buildGenerator(Type.ONECHAR);
        
        EntitySpeed es = new EntitySpeed(g1);
        
        Logger.getInstance().error("time off");
        Logger.getInstance().setTime(new TimeOn());
        Logger.getInstance().error("time on");
        
        hm.ReadScore();
        hm.SetEntry(0, new HighscoreEntry("laimetojas", 4444444));
        hm.SetEntry(1, new HighscoreEntry("test", 3333333));
        hm.SetEntry(2, new HighscoreEntry("test", 222222));
        hm.SetEntry(3, new HighscoreEntry("test", 33333));
        hm.SetEntry(4, new HighscoreEntry("test", 2222));
        hm.SetEntry(5, new HighscoreEntry("test", 333));
        hm.SetEntry(6, new HighscoreEntry("test", 22));
        hm.SetEntry(7, new HighscoreEntry("test", 15));
        hm.SetEntry(8, new HighscoreEntry("test", 10));
        hm.SetEntry(9, new HighscoreEntry("test", 5));
        hm.SaveEntry(new HighscoreEntry("labas", 55555));
        hm.SaveScore();
        
        
        for (Iterator iter = hm.getIterator(); iter.hasNext();) {
            System.out.println(iter.next());
        }
        
        g.setScore(123);
        System.out.println("Before: " + g.getScore());
        gm.storeCommand(new ResetGame(g));
        gm.storeCommand(new AddScoreEntity(g, es));
        gm.executeCommand();
        System.out.println("After: " + g.getScore());
    }
}
