package util;

public class Logger {
    private boolean warningOn, debugOn, errorOn, infoOn;
    private TimeState ts;
    private final String warnPrefix = "|W| ";
    private final String debugPrefix = "|D| ";
    private final String errorPrefix = "|E| ";
    private final String infoPrefix = "|I| ";
    private static Logger instance = null;
    
    private Logger() {
        this.warningOn = true;
        this.debugOn = false;
        this.errorOn = true;
        this.infoOn = false;
        this.ts = new TimeOff();
    }
    
    public TimeState setTime(TimeState val) {
        TimeState ret = this.ts;
        this.ts = val;
        return ret;
    }
    
    public boolean setWarning(boolean val) {
        boolean ret = this.warningOn;
        this.warningOn = val;
        return ret;
    }
    
    public boolean setDebug(boolean val) {
        boolean ret = this.debugOn;
        this.debugOn = val;
        return ret;
    }   
         
    public boolean setError(boolean val) {
        boolean ret = this.errorOn;
        this.errorOn = val;
        return ret;
    }   
    
    public boolean setInfo(boolean val) {
        boolean ret = this.infoOn;
        this.infoOn = val;
        return ret;
    }   
    
    private void printMsg(String msg) {
        ts.printMsg(msg);
    }
    
    public void warning(String msg) {
        if (!this.warningOn)
            return;
        
        printMsg(this.warnPrefix + msg);
    }
    
    public void debug(String msg) {
        if (!this.debugOn)
            return;
        
        printMsg(this.debugPrefix + msg);
    }
    
    public void info(String msg) {
        if (!this.infoOn)
            return;
        
        printMsg(this.infoPrefix + msg);
    }
    
    public void error(String msg) {
        if (!this.errorOn)
            return;
        
        printMsg(this.errorPrefix + msg);
    }
    
    public static Logger getInstance() {
        if (instance == null)
            instance = new Logger();
        return instance;
    }
}
