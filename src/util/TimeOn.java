package util;

import java.util.Calendar;

public class TimeOn implements TimeState {
    @Override
    public void printMsg(String msg) {
        Calendar c = Calendar.getInstance();
        System.err.printf("[%tF %tH:%tM:%tS] %s%n", c, c, c, c, msg);
    }
    
}
