package state;

import score.Entity;

public class AddScoreEntity implements Command {
    private Game g;
    private Entity ent;
    
    public AddScoreEntity(Game g, Entity ent) {
        this.g = g;
        this.ent = ent;
    }
    
    @Override
    public void execute() {
        this.g.setScore(this.g.getScore() + (int)this.ent.getPoints());
    }
}
