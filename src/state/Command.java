package state;

public interface Command {
    void execute();
}
