package state;

public class ResetGame implements Command {
    private Game g;
    
    public ResetGame(Game g) {
        this.g = g;
    }
    
    @Override
    public void execute() {
        this.g.setNumWords(0);
        this.g.setScore(0);
        this.g.setTimesecs(0.0);
    }
}
