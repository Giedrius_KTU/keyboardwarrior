package state;

public class GameState {
    private Integer score, timesecs, words;
    private Boolean training;
    
    public GameState(boolean training) {
        this.score = 0;
        this.timesecs = 0;
        this.words = 0;
        this.training = training;
    }
    
    public Integer getScore() {
        return this.score;
    }
    
    public Integer getWords() {
        return this.words;
    }
    
    public void setScore(Integer score) {
        this.score = score;
    }
    
    public void setWords(Integer words) {
        this.words = words;
    }
    
    public Boolean getTraining() {
        return this.training;
    }
    
    public void setTraining(Boolean training) {
        this.training = training;
    }
    
    public Integer getTimesecs() {
        return this.timesecs;
    }
    
    public void setTimesecs(Integer timesecs) {
        this.timesecs = timesecs;
    }
}
