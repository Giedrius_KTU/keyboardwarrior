package state;

public class GameStateStatistics { 
    public static double scorePerSec(GameState state) {
        return state.getScore() / state.getTimesecs();
    }
    
    public static double wordsPerSec(GameState state) {
        return state.getWords()/ state.getTimesecs();
    } 
}
