package state;

import score.HighscoreManager;
import wordgenerator.Generator;

public class Game {
    private Integer score, numwords;
    private final Boolean training;
    private final Type mode;
    private double timesecs;
    
    public Game(boolean training, Type mode) {
        this.score = 0;
        this.timesecs = 0;
        this.numwords = 0;
        this.training = training;
        this.mode = mode;
    }
    
    public Integer getScore() {
        return this.score;
    }
    
    public Integer getNumWords() {
        return this.numwords;
    }
    
    public void setScore(Integer score) {
        this.score = score;
    }
    
    public void setNumWords(Integer words) {
        this.numwords = words;
    }
    
    public Boolean getTraining() {
        return this.training;
    }
    
    public Type getMode() {
        return this.mode;
    }
    
    public double getTimesecs() {
        return this.timesecs;
    }
    
    public void setTimesecs(double timesecs) {
        this.timesecs = timesecs;
    }
    
    public double scorePerSec() {
        return this.getScore() / this.getTimesecs();
    }
    
    public double wordsPerSec() {
        return this.getNumWords()/ this.getTimesecs();
    } 
}
