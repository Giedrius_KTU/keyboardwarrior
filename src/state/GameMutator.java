package state;

import java.util.ArrayList;
import java.util.List;

public class GameMutator {
    private List<Command> history = new ArrayList<>();
    
    public void storeCommand(Command cmd) {
        this.history.add(cmd);
    }
    
    public void executeCommand() {
        for (Command cmd : history) {
            cmd.execute();
        }
        history.clear();
    }
    
}
